#!/usr/bin/env sh
set -euo pipefail

echo "CLJ"
clojure --main clj.hello

echo "CLJC on JVM"
clojure --main cljc.hello

echo "CLJS"
clojure -A:build-for-cljs &&
	node ./.cljs_node_repl/main.js

echo "CLJC on Node"
clojure -A:build-for-cljc-througth-cljs &&
	node ./.cljs_node_repl/main.js
