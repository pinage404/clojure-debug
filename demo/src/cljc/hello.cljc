(ns cljc.hello
  (:require
   [debug.print
    :refer [pretty-print
            spy
            group-begin group-begin-collapsed group-end
            print-group]]
   [debug.macro
    :refer [fn-input fn-output
            fn-io fn-group fn-group-io
            defn-io defn-group defn-group-io
            defn-generate-test]
    :refer-macros []]))


(defn-group-io hello
  []
  (println "Hello world"))


#?(:cljs (hello)
   :clj  (defn -main [& args]
           (hello)))
