(ns clj.hello
  (:use [debug.print])
  (:use [debug.macro]))


(defn-group-io hello
  []
  (println "Hello world"))


(defn -main [& args]
  (hello))
