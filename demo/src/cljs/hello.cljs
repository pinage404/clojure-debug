(ns cljs.hello
  (:require
   [debug.print :refer [pretty-print
                        spy
                        group-begin group-begin-collapsed group-end
                        print-group]]
   [debug.macro :refer-macros [fn-input fn-output
                               fn-io fn-group fn-group-io
                               defn-io defn-group defn-group-io
                               defn-generate-test]]))


(defn-group-io hello
  []
  (println "Hello world"))


(hello)
