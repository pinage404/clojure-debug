#!/usr/bin/env sh
set -eu

OUTPUT=$(node ./.cljs_node_repl/main.js)

echo "$OUTPUT"

echo "$OUTPUT" |
	grep \
		-F \
		-x \
		-q \
		"0 failures, 0 errors."
# -F = --fixed-regexp
# -x = --line-regexp
# -q = --quiet
