#!/usr/bin/env sh
set -eu

clojure -A:build-test-for-cljs

./script/run-generated-test-cljs.sh
