# Changelog

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/)
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html)

## [Unreleased]

[Unreleased]: https://gitlab.com/pinage404/clojure-debug/-/compare/version/1.5.1...HEAD

## [1.5.1] - 2021-03-17

### Fix

* `pretty-print` should use `console.log` in CLJS

[1.5.1]: https://gitlab.com/pinage404/clojure-debug/-/compare/version/1.5.0...version/1.5.1
