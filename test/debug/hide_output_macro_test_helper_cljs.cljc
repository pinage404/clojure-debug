(ns debug.hide-output-macro-test-helper-cljs
  "macro are used at compile time
   with Clojure -> JVM -> CLJ

   but the result is used at runtime
   with ClojureScript -> JavaScript -> CLJS

   have to split files to have 1 implementation for 1 sub language")

(defmacro hide-output
  [& body]
  `(binding [cljs.core/*print-fn* (constantly nil)]
     ~@body))
