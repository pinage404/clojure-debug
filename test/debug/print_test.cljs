(ns debug.print-test
  (:require
   [cljs.test :refer [deftest testing is]]
   [debug.split-output-macro-test-helper :refer-macros [split-output]]
   [debug.hide-output-macro-test-helper-cljs :refer-macros [hide-output]]
   [debug.print :refer [pretty-print
                        spy
                        group-end]]))


(deftest pretty-print-test
  (testing "should return the given input"
    (is (= "something"
           (hide-output (pretty-print "something"))))))


(deftest spy-test
  (testing "should return the given input"
    (is (= "something"
           (hide-output (spy "something"))))))


(deftest group-end-test
  (testing "should display the given label with a prefix"
    (is (= [""]
           (split-output (group-end "something"))))))
