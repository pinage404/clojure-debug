(ns debug.macro-test
  (:require
   [clojure.test :refer [deftest testing is]]
   [debug.split-output-macro-test-helper
    :refer        [split-output]
    :refer-macros [split-output]]
   #?(:clj  [debug.hide-output-macro-test-helper-clj  :refer        [hide-output]])
   #?(:cljs [debug.hide-output-macro-test-helper-cljs :refer-macros [hide-output]])
   [debug.macro
    :refer        [fn-input fn-output
                   fn-io fn-group fn-group-io
                   defn-io defn-group defn-group-io
                   defn-generate-test]
    :refer-macros []]))


(def optional-js-mark
  #?(:clj  ""
     :cljs "#js "))


(defn mock-group-begin
  [label]
  (println "prefix" label))

(defn mock-group-end
  [label]
  (println "suffix" label))


(deftest fn-input-tests
  (testing "fn-input without arguments"
    (let [fixture (fn-input "fn-input label without arguments"
                            []
                            (println "body")
                            (* 2 3))]
      #?(:clj
         (testing "display input"
           (is (= [(str "fn-input label without arguments input: " optional-js-mark "[]")
                   "body"]
                  (split-output (fixture))))))
      (testing "execute and return the body"
        (is (= 6
               (hide-output (fixture)))))))

  (testing "fn-input with arguments"
    (let [fixture (fn-input "fn-input label with arguments"
                            [x]
                            (println "body")
                            (* x 3))]
      #?(:clj
         (testing "display input"
           (is (= [(str "fn-input label with arguments input: " optional-js-mark "[2]")
                   "body"]
                  (split-output (fixture 2))))))
      (testing "execute and return the body"
        (is (= 6
               (hide-output (fixture 2))))))))


(deftest fn-output-tests
  (testing "fn-output without arguments"
    (let [fixture (fn-output "fn-output label without arguments"
                             []
                             (println "body")
                             (* 2 3))]
      #?(:clj
         (testing "display outut"
           (is (= ["body"
                   "fn-output label without arguments output: 6"]
                  (split-output (fixture))))))
      (testing "execute and return the body"
        (is (= 6
               (hide-output (fixture)))))))

  (testing "fn-output with arguments"
    (let [fixture (fn-output "fn-output label with arguments"
                             [x]
                             (println "body")
                             (* x 3))]
      #?(:clj
         (testing "display outut"
           (is (= ["body"
                   "fn-output label with arguments output: 6"]
                  (split-output (fixture 2))))))
      (testing "execute and return the body"
        (is (= 6
               (hide-output (fixture 2))))))))


(deftest fn-io-tests
  (testing "fn-io without arguments"
    (let [fixture (fn-io "fn-io label without arguments"
                         []
                         (println "body")
                         (* 2 3))]
      #?(:clj
         (testing "display input and outut"
           (is (= [(str "fn-io label without arguments input: " optional-js-mark "[]")
                   "body"
                   "fn-io label without arguments output: 6"]
                  (split-output (fixture))))))
      (testing "execute and return the body"
        (is (= 6
               (hide-output (fixture)))))))

  (testing "fn-io with arguments"
    (let [fixture (fn-io "fn-io label with arguments"
                         [x]
                         (println "body")
                         (* x 3))]
      #?(:clj
         (testing "display input and outut"
           (is (= [(str "fn-io label with arguments input: " optional-js-mark "[2]")
                   "body"
                   "fn-io label with arguments output: 6"]
                  (split-output (fixture 2))))))
      (testing "execute and return the body"
        (is (= 6
               (hide-output (fixture 2))))))))


(deftest fn-group-tests
  (with-redefs [debug.print/group-begin mock-group-begin
                debug.print/group-end mock-group-end]
    (testing "fn-group without arguments"
      (let [fixture (fn-group "fn-group label without arguments"
                              []
                              (println "body")
                              (* 2 3))]
        #?(:clj
           (testing "display input and outut"
             (is (= ["prefix fn-group label without arguments"
                     "body"
                     "suffix fn-group label without arguments"]
                    (split-output (fixture))))))
        (testing "execute and return the body"
          (is (= 6
                 (hide-output (fixture)))))))

    (testing "fn-group with arguments"
      (let [fixture (fn-group "fn-group label with arguments"
                              [x]
                              (println "body")
                              (* x 3))]
        #?(:clj
           (testing "display input and outut"
             (is (= ["prefix fn-group label with arguments"
                     "body"
                     "suffix fn-group label with arguments"]
                    (split-output (fixture 2))))))
        (testing "execute and return the body"
          (is (= 6
                 (hide-output (fixture 2)))))))))


(deftest fn-group-io-tests
  (with-redefs [debug.print/group-begin mock-group-begin
                debug.print/group-end mock-group-end]
    (testing "fn-group-io without arguments"
      (let [fixture (fn-group-io "fn-group-io label without arguments"
                                 []
                                 (println "body")
                                 (* 2 3))]
        #?(:clj
           (testing "display input and outut"
             (is (= ["prefix fn-group-io label without arguments"
                     (str "fn-group-io label without arguments input: " optional-js-mark "[]")
                     "body"
                     "fn-group-io label without arguments output: 6"
                     "suffix fn-group-io label without arguments"]
                    (split-output (fixture))))))
        (testing "execute and return the body"
          (is (= 6
                 (hide-output (fixture)))))))

    (testing "fn-group-io with arguments"
      (let [fixture (fn-group-io "fn-group-io label with arguments"
                                 [x]
                                 (println "body")
                                 (* x 3))]
        #?(:clj
           (testing "display input and outut"
             (is (= ["prefix fn-group-io label with arguments"
                     (str "fn-group-io label with arguments input: " optional-js-mark "[2]")
                     "body"
                     "fn-group-io label with arguments output: 6"
                     "suffix fn-group-io label with arguments"]
                    (split-output (fixture 2))))))
        (testing "execute and return the body"
          (is (= 6
                 (hide-output (fixture 2)))))))))


(defn-io fixture-defn-io-without-arguments
  []
  (println "body")
  (* 2 3))

(defn-io fixture-defn-io-with-arguments
  [x]
  (println "body")
  (* x 3))

(deftest defn-io-tests
  (testing "defn-io without arguments"
    #?(:clj
       (testing "display input and outut"
         (is (= [(str "fixture-defn-io-without-arguments input: " optional-js-mark "[]")
                 "body"
                 "fixture-defn-io-without-arguments output: 6"]
                (split-output (fixture-defn-io-without-arguments))))))
    (testing "execute and return the body"
      (is (= 6
             (hide-output (fixture-defn-io-without-arguments))))))

  (testing "defn-io with arguments"
    #?(:clj
       (testing "display input and outut"
         (is (= [(str "fixture-defn-io-with-arguments input: " optional-js-mark "[2]")
                 "body"
                 "fixture-defn-io-with-arguments output: 6"]
                (split-output (fixture-defn-io-with-arguments 2))))))
    (testing "execute and return the body"
      (is (= 6
             (hide-output (fixture-defn-io-with-arguments 2)))))))


(defn-group fixture-defn-group-without-arguments
  []
  (println "body")
  (* 2 3))

(defn-group fixture-defn-group-with-arguments
  [x]
  (println "body")
  (* x 3))

(deftest defn-group-tests
  (with-redefs [debug.print/group-begin mock-group-begin
                debug.print/group-end mock-group-end]
    (testing "defn-group without arguments"
      #?(:clj
         (testing "display input and outut"
           (is (= ["prefix fixture-defn-group-without-arguments"
                   "body"
                   "suffix fixture-defn-group-without-arguments"]
                  (split-output (fixture-defn-group-without-arguments))))))
      (testing "execute and return the body"
        (is (= 6
               (hide-output (fixture-defn-group-without-arguments))))))

    (testing "defn-group with arguments"
      #?(:clj
         (testing "display input and outut"
           (is (= ["prefix fixture-defn-group-with-arguments"
                   "body"
                   "suffix fixture-defn-group-with-arguments"]
                  (split-output (fixture-defn-group-with-arguments 2))))))
      (testing "execute and return the body"
        (is (= 6
               (hide-output (fixture-defn-group-with-arguments 2))))))))


(defn-group-io fixture-defn-group-io-without-arguments
  []
  (println "body")
  (* 2 3))

(defn-group-io fixture-defn-group-io-with-arguments
  [x]
  (println "body")
  (* x 3))

(deftest defn-group-io-tests
  (with-redefs [debug.print/group-begin mock-group-begin
                debug.print/group-end mock-group-end]
    (testing "defn-group-io without arguments"
      #?(:clj
         (testing "display input and outut"
           (is (= ["prefix fixture-defn-group-io-without-arguments"
                   (str "fixture-defn-group-io-without-arguments input: " optional-js-mark "[]")
                   "body"
                   "fixture-defn-group-io-without-arguments output: 6"
                   "suffix fixture-defn-group-io-without-arguments"]
                  (split-output (fixture-defn-group-io-without-arguments))))))
      (testing "execute and return the body"
        (is (= 6
               (hide-output (fixture-defn-group-io-without-arguments))))))

    (testing "defn-group-io with arguments"
      #?(:clj
         (testing "display input and outut"
           (is (= ["prefix fixture-defn-group-io-with-arguments"
                   (str "fixture-defn-group-io-with-arguments input: " optional-js-mark "[2]")
                   "body"
                   "fixture-defn-group-io-with-arguments output: 6"
                   "suffix fixture-defn-group-io-with-arguments"]
                  (split-output (fixture-defn-group-io-with-arguments 2))))))
      (testing "execute and return the body"
        (is (= 6
               (hide-output (fixture-defn-group-io-with-arguments 2))))))))


(defn-generate-test a-legacy-function-without-arguments-and-without-test
  []
  (* 2 3))

(defn-generate-test a-legacy-function-with-arguments-and-without-test
  [a b]
  (* a b))

(deftest defn-generate-test-tests
  (testing "defn-generate-test without arguments"
    #?(:clj
       (testing "display a basic form to add test with real data"
         (is (= ["(deftest a-legacy-function-without-arguments-and-without-test-test"
                 "  (testing \"write the rule of this case here\""
                 "    (is (= 6"
                 "           (a-legacy-function-without-arguments-and-without-test)"
                 "    ))"
                 "  )"
                 ")"]
                (split-output (a-legacy-function-without-arguments-and-without-test))))))
    (testing "execute and return the body"
      (is (= 6
             (hide-output (a-legacy-function-without-arguments-and-without-test))))))

  (testing "defn-generate-test with arguments"
    #?(:clj
       (testing "display a basic form to add test with real data"
         (is (= ["(deftest a-legacy-function-with-arguments-and-without-test-test"
                 "  (testing \"write the rule of this case here\""
                 "    (is (= 6"
                 "           (a-legacy-function-with-arguments-and-without-test 2 3)"
                 "    ))"
                 "  )"
                 ")"]
                (split-output (a-legacy-function-with-arguments-and-without-test 2 3))))))
    (testing "execute and return the body"
      (is (= 6
             (hide-output (a-legacy-function-with-arguments-and-without-test 2 3)))))))
