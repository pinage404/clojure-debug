(ns debug.hide-output-macro-test-helper-clj
  "macro are used at compile time
   with Clojure -> JVM -> CLJ

   but the result is used at runtime
   with Clojure -> JVM -> CLJ

   have to split files to have 1 implementation for 1 sub language")

(defmacro hide-output
  [& body]
  `(binding [*out* (new java.io.StringWriter)]
     ~@body))
