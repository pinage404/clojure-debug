(ns debug.split-output-macro-test-helper
  (:require
   [clojure.string :refer [split-lines]]))

(defmacro split-output
  [& body]
  `(split-lines
    (with-out-str
      ~@body)))
