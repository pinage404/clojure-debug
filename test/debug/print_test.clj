(ns debug.print-test
  (:require
   [clojure.test :refer [deftest testing is are]]
   [debug.split-output-macro-test-helper :refer [split-output]]
   [debug.hide-output-macro-test-helper-clj :refer [hide-output]]
   [debug.print :refer [pretty-print
                        spy
                        group-begin group-begin-collapsed group-end
                        print-group]]))


(deftest pretty-print-test
  (testing "should display in a pretty way input"
    (is (= ["\"something\""]
           (split-output (pretty-print "something")))))

  (testing "should return the given input"
    (is (= "something"
           (hide-output (pretty-print "something"))))))


(deftest spy-test
  (testing "should display the type then display in a pretty way input"
    (is (= ["java.lang.String"
            "\"something\""]
           (split-output (spy "something")))))

  (testing "should return the given input"
    (is (= "something"
           (hide-output (spy "something"))))))


(deftest group-begin-test
  "in CLJ, group-begin and group-begin-collapsed should be the same"
  (testing "should display the given label with a prefix"
    (are [f]
         (= ["<<< something"]
            (split-output (f "something")))
      group-begin
      group-begin-collapsed)))


(deftest group-end-test
  (testing "should display the given label with a prefix"
    (is (= [">>> something"]
           (split-output (group-end "something"))))))


(deftest print-group-test
  (testing "should display the given value in a group"
    (is (= ["<<< some label"
            "\"something\""
            ">>> some label"]
           (split-output (print-group "some label" "something")))))

  (testing "should return the given input"
    (is (= "something"
           (hide-output (print-group "some label" "something"))))))
