(ns test-runner
  (:require
   [cljs.test :refer-macros [run-tests]]
   [debug.macro-test]
   [debug.print-test]))

(run-tests
 'debug.macro-test
 'debug.print-test)
