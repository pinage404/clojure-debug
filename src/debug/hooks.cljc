(ns debug.hooks
  "Internal module"
  (:require
   [clojure.string :refer [join]]
   [debug.print :refer [pretty-print
                        group-begin group-end]]))

(defn hook-display-input
  [{:keys [label args]}]
  (print label "input: ")
  (pretty-print args))

(defn hook-display-output
  [{:keys [label result]}]
  (print label "output: ")
  (pretty-print result))

(defn hook-group-begin
  [{:keys [label]}]
  (group-begin label))

(defn hook-group-end
  [{:keys [label]}]
  (group-end label))

(defn hook-generate-test
  [{:keys [fn-name args result]}]
  (let [test-array [["(deftest " fn-name "-test"]
                    ["  (testing \"write the rule of this case here\""]
                    ["    (is (= " result]
                    ["           (" (join " " (cons fn-name args)) ")"]
                    ["    ))"]
                    ["  )"]
                    [")"]]
        test-str (join "\n" (map join test-array))]
    (println test-str)))
