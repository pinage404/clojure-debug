(ns debug.print
  "Display stuff in a pretty way (depending on the platform)"
  #?(:clj (:require
           [clojure.pprint :refer [pprint]])))

(defn pretty-print
  "Display the given value, in a pretty way (depending on the platform) and return it

  Usage:

  ```diff
   (fn [arg]
  -  (do-stuff arg))
  +  (pretty-print (do-stuff arg)))
  ```"
  [arg]
  #?(:clj  (pprint arg)
     :cljs (js/console.log (clj->js arg)))
  arg)

(defn spy
  "Display the type and the value of the given value and return it

  Usage:

  ```diff
   (fn [arg]
  -  (do-stuff arg))
  +  (spy (do-stuff arg)))
  ```"
  [arg]
  (pretty-print (type arg))
  (pretty-print arg))

(defn group-begin
  [name]
  #?(:clj  (println "<<<" name)
     :cljs (js/console.group name)))

(defn group-begin-collapsed
  [name]
  #?(:clj  (println "<<<" name)
     :cljs (js/console.groupCollapsed name)))

(defn group-end
  [name]
  #?(:clj  (println ">>>" name)
     :cljs (js/console.groupEnd)))

(defn print-group
  "Display a value in a grouped output and return it

  Usage:

  ```diff
   (fn [arg]
  -  (do-stuff arg))
  +  (print-group \"label\" (do-stuff arg)))
  ```"
  [label arg]
  (group-begin-collapsed label)
  (pretty-print arg)
  (group-end label)
  arg)
