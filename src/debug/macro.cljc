(ns debug.macro
  (:require
   [debug.hooks :refer [hook-display-input
                        hook-display-output
                        hook-group-begin
                        hook-group-end
                        hook-generate-test]]))


(defmacro fn-input
  "Log input of a function

  Usage:

  ```diff
  -(fn
  +(fn-input \"some label to help while debugging\"
     [arg]
     (do-stuff arg))
  ```"
  [label args & body]
  (let [context {:label label
                 :args args}]
    `(fn [~@args]
       (hook-display-input ~context)
       ~@body)))


(defmacro fn-output
  "Log output of a function

  Usage:

  ```diff
  -(fn
  +(fn-output \"some label to help while debugging\"
     [arg]
     (do-stuff arg))
  ```"
  [label args & body]
  (let [context {:label label
                 :args args}]
    `(fn [~@args]
       (let [result# (do ~@body)
             result-context# (assoc ~context :result result#)]
         (hook-display-output result-context#)
         result#))))


(defmacro fn-io
  "Log input and output of a function

  Usage:

  ```diff
  -(fn
  +(fn-io \"some label to help while debugging\"
     [arg]
     (do-stuff arg))
  ```"
  [label args & body]
  (let [context {:label label
                 :args args}]
    `(fn [~@args]
       (hook-display-input ~context)
       (let [result# (do ~@body)
             result-context# (assoc ~context :result result#)]
         (hook-display-output result-context#)
         result#))))


(defmacro fn-group
  "Wrap function console group

  Usage:

  ```diff
  -(fn
  +(fn-group \"some label to help while debugging\"
     [arg]
     (do-stuff arg))
  ```"
  [label args & body]
  (let [context {:label label
                 :args args}]
    `(fn [~@args]
       (hook-group-begin ~context)
       (let [result# (do ~@body)
             result-context# (assoc ~context :result result#)]
         (hook-group-end result-context#)
         result#))))


(defmacro fn-group-io
  "Wrap function console group and log input and output

  Usage:

  ```diff
  -(fn
  +(fn-group-io \"some label to help while debugging\"
     [arg]
     (do-stuff arg))
  ```"
  [label args & body]
  (let [context {:label label
                 :args args}]
    `(fn [~@args]
       (hook-group-begin ~context)
       (hook-display-input ~context)
       (let [result# (do ~@body)
             result-context# (assoc ~context :result result#)]
         (hook-display-output result-context#)
         (hook-group-end result-context#)
         result#))))


(defmacro defn-io
  "Log input and output of a function definition

  Usage:

  ```diff
  -(defn my-function
  +(defn-io my-function
     [arg]
     (do-stuff arg))
  ```"
  [fn-name & decls]
  (let [fn-name-str (str fn-name)]
    `(def ~fn-name
       (fn-io ~fn-name-str ~@decls))))


(defmacro defn-group
  "Wrap function definition console group

  Usage:

  ```diff
  -(defn my-function
  +(defn-group my-function
     [arg]
     (do-stuff arg))
  ```"
  [fn-name & decls]
  (let [fn-name-str (str fn-name)]
    `(def ~fn-name
       (fn-group ~fn-name-str ~@decls))))


(defmacro defn-group-io
  "Wrap function definition console group and log input and output

  Usage:

  ```diff
  -(defn my-function
  +(defn-group-io my-function
     [arg]
     (do-stuff arg))
  ```"
  [fn-name & decls]
  (let [fn-name-str (str fn-name)]
    `(def ~fn-name
       (fn-group-io ~fn-name-str ~@decls))))


(defmacro defn-generate-test
  "Wrap function definition that output a test definition for real data

  Usage:

  ```diff
  -(defn my-function
  +(defn-generate-test my-function
     [arg]
     (do-stuff arg))
  ```"
  [fn-name args & body]
  (let [fn-name-str (str fn-name)]
    `(defn ~fn-name
       [~@args]
       (let [result# (do ~@body)
             result-context# {:fn-name ~fn-name-str
                              :args [~@args]
                              :result result#}]
         (hook-generate-test result-context#)
         result#))))
