# Clojure Debug

[![Repository](https://img.shields.io/badge/GitLab-pinage404%2Fclojure--debug-fa7035.svg?logo=GitLab)](https://gitlab.com/pinage404/clojure-debug)
[![Clojars Project](https://img.shields.io/clojars/v/pinage404/debug.svg?logo=Clojure)](https://clojars.org/pinage404/debug)
[![Documentation](https://img.shields.io/badge/Documentation-pinage404%2Fdebug-357edd.svg?logo=Clojure)](https://cljdoc.org/d/pinage404/debug)

## Install

Follow [instructions on the Clojars page](https://clojars.org/pinage404/debug)

## Usage

On `fn` or `defn` append `-input`, `-output`, `-io`, `-group` or `-group-io` to wrap function to log stuff in the console

```diff
-(ns hello)
+(ns hello
+  (:use [debug.macro]))
 
-(defn my-function
+(defn-group-io my-function
   [arg]
   (do-stuff arg))
```

Check more on [the documentation](https://cljdoc.org/d/pinage404/debug)

### CLJ

Just use `:use`, it will import everything in the scope

It's "dirty" but short and simple : perfect while debugging

```diff
-(ns hello)
+(ns hello
+  (:use [debug.print])
+  (:use [debug.macro]))
```

### CLJS

In CLJS, it seems that we have to explicit refer to everything that we want to use

Looking for a shorter way [contributions are welcome](#contributing)

```diff
-(ns hello)
+(ns hello
+  (:require
+   [debug.print :refer [pretty-print
+                        spy
+                        group-begin group-begin-collapsed group-end
+                        print-group]]
+   [debug.macro :refer-macros [fn-input fn-output
+                               fn-io fn-group fn-group-io
+                               defn-io defn-group defn-group-io
+                               defn-generate-test]]))
```

### CLJC

Meld both CLJ and CLJS

Looking for a shorter way [contributions are welcome](#contributing)

```diff
-(ns hello)
+(ns hello
+  (:require
+   [debug.print :refer [pretty-print
+                        spy
+                        group-begin group-begin-collapsed group-end
+                        print-group]]
+   [debug.macro
+    :refer [fn-input fn-output
+            fn-io fn-group fn-group-io
+            defn-io defn-group defn-group-io
+            defn-generate-test]
+    :refer-macros []]))
```

### CLJR

Maybe it works, maybe it doesn't

I haven't tested it, i don't know how to test it

[Contributions are welcome](#contributing)

## Demo

Check [the demo folder](./demo/README.md)

## Contributing

Any contributions ([feedback, bug report](https://gitlab.com/pinage404/clojure-debug/-/issues), [merge request](https://gitlab.com/pinage404/clojure-debug/-/merge_requests) ...) are welcome

### Test

```sh
clojure -A:test-clj
./script/test-cljs.sh
```

### Deploy a new version

```sh
git tag vX.Y.Z && git push origin vX.Y.Z
```

## Backlog

* Clojars
  * [ ] [Description](https://github.com/applied-science/deps-library/issues/3)
* Backlog
  * [ ] docstring support
  * [ ] debug rum components
    * using why-did-you-render ?
    * in another package ?
  * [ ] `fn-*` macros, label should be optional
  * [ ] find a way to automatically inject without the need to add `:use` or `:require`

## Changelog

[Changelog](./CHANGELOG.md)

## License

[MIT License](./LICENSE)
